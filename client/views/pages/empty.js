/**
 * Created by andrey on 15.03.2019.
 */
Template.empty.onRendered(function() {
    let slideHeight = $(window).height();
    $('.wrapper-empty').css('height', slideHeight);
    $(window).resize(function () {
        $('.wrapper-empty').css('height', slideHeight);
    });
})