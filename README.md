# Base project for meteor

Meteor project for decrease your time for setting new projects each time.

# File structure

```
.
|--.meteor
|--.settings-local
    |--settings.json
|--client
    |--global
        |--helpers
           |--consoleLog.js
        |--methods
           |--regexps.js
        |--settings
    |--libs
    |--stylesheets
    |--views
        |--layouts
        |--modules
        |--pages
        |--head.js
|--common
    |--database
    |--router.js
|--public
    |--img
|--server
    |--methods
    |--publish
|--.gitignore
|--package.json
|--package-lock.json

```

# Pre installed packages

```
meteor-base@1.2.0
mobile-experience@1.0.5
mongo@1.3.0
blaze-html-templates@1.0.4
reactive-var
tracker@1.1.3
standard-minifier-css@1.3.5
standard-minifier-js@2.2.0
es5-shim@4.6.15
ecmascript@0.9.0
shell-server@0.3.0
insecure@1.0.7
iron:router
accounts-password
alexwine:bootstrap-4
fortawesome:fontawesome
session
less

```

# Author
Tech Creation Company http://tcc.dev